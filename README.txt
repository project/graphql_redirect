CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The GraphQL Redirect module is a simple module that extends the contributed
GraphQL module. It replaces default route handling so that GraphQL query
follows redirects created by Redirect module and returns the query result for
the destination route. It supports internationalization and chained redirects.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/graphql_redirect

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/graphql_redirect


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the GraphQL Redirect module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

Important: Until #2962966: findByRedirect doesn't handle cacheable dependencies
correctly is resolved, GraphQL Redirect will require this patch to work
correctly:
 * https://www.drupal.org/project/redirect/issues/2962966


CONFIGURATION
-------------

Navigate to Administration > Extend and enable the module.

Example: Imagine having content page with alias "/test-page" and a redirect from
"/test-page-redirect" to "/test-page". With built-in GraphQL route handling,
querying route "/test-page-redirect" would return null, since it does not
recognize redirects. With GraphQL Redirect, querying:

```
{
  route(path:"/test-page-redirect") {
    ... on EntityCanonicalUrl {
      pathAlias
      entity {
        entityId
        entityLabel
      }
    }
  }
}
```
returns:
```
{
  "data": {
    "route": {
      "pathAlias": "/test-page",
      "entity": {
        "entityId": "1",
        "entityLabel": "Test page"
      }
    }
  }
}
```

If pathAlias is different than path provided in the query parameters, client
knows there was a redirect and needs to change URL to the destination one.


MAINTAINERS
-----------

 * peter.keppert - https://www.drupal.org/u/peterkeppert
